import numpy as np


class PressureVessel(object):

    def __init__(self, pressure=4e6, density=7.8e3):
        self.pressure = pressure
        self.density = density

    def eval_mass(self, radius, length, t_head, t_sheet):
        cylinder_shell_volume = np.pi * length * ((t_sheet + radius) ** 2 - radius ** 2)
        spherical_heads_volume = 4.0 / 3.0 * np.pi * ((radius + t_head) ** 3 - radius ** 3)
        material_volume = cylinder_shell_volume + spherical_heads_volume
        return self.density * material_volume

    def eval_stress(self, radius, t_head, t_sheet):
        assert t_head != 0, "Thickness of hemi-spherical heads should be a positive number, given t_head=%f" % t_head
        assert t_sheet != 0, "Thickness of cylindrical shell should be a positive number, given t_sheet=%f" % t_sheet
        spherical_heads_pressure = self.pressure * radius / (2.0 * t_head)
        cylinder_shell_pressure = self.pressure * radius / t_sheet
        return max(spherical_heads_pressure, cylinder_shell_pressure)

    def eval_volume(self, radius, length):
        return 4.0 / 3.0 * np.pi * (radius ** 3) + np.pi * (radius ** 2) * length
